
#include "tprintf.h"

int main() {
    int x;
    std::string s("moomoocow");
    tprintf("hello\n");
    tprintf("five = %\n") % 5;
    tprintf("===%===\n% = %\n&x = %\npadded int = %5\nfloat precision = %.4\n") % s, "hello", 5ll, &x, 4, 8.5;
    tprintf("format with string %.5") % "moo";
}

