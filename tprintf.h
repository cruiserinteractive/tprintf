/** $Id$ $URL$ */
#ifndef TPRINTF_DOT_AITCH
#define TPRINTF_DOT_AITCH
/**\file tprintf.h
 *
 * Templatised printf-like functions
 */

#include <iostream>
#include <string>
#include <string.h>
#include <stdio.h>

#ifndef NUMFMT_CHARS
#define NUMFMT_CHARS ".#-+0123456789"
#endif


class SDL_mutex;

class Tprintf {
public:
    typedef std::string S;
    typedef S::iterator SIT;
    typedef S::const_iterator SCIT;
protected:
    std::ostream* out;
    S format;
    S numfmt;
    SIT pos;
    SIT nonumreturn;
    bool prog;
    SDL_mutex *mut;
    const char* lastspec;
    void set(std::ostream& o, const S& fmt);
    void check();
    bool next(S::value_type &c);
    bool after(S::value_type &c);

    void fillnumfmt() {
        S::value_type n = '?';
        nonumreturn = pos;
        numfmt = "";
        for (; next(n) && strchr(NUMFMT_CHARS, n); ++pos)
            numfmt += n;
    }

    SIT find();
    SIT find(const S::value_type c);
    template <class T>
        void dumpnum(T arg, const char* spec);
    template <class T>
        void dump(T arg);
    /*
    template <class T>
        void dump(T *arg);
    template <class T>
    void dump(const T *arg);
    */
public:
    Tprintf(std::ostream& o, const S& fmt = "%");
    //Tprintf(FILE* f, const S& fmt = "%");
    Tprintf(const S& format = "%");
    Tprintf(const Tprintf& rhs);
    Tprintf& operator=(const Tprintf& rhs);
    ~Tprintf();
    Tprintf& operator()(std::ostream& o, const S& fmt = "%");
    Tprintf& operator()(const S& format = "%");
    void rewind();
    template <class T>
        Tprintf& process(T arg);
    template <class T>
        Tprintf& operator<<(T arg) {return process(arg);}
    template <class T>
        Tprintf& operator%(T arg) {return process(arg);}
    template <class T>
        Tprintf& operator,(T arg) {return process(arg);}

    //Tprintf& operator()(std::ostream& o, const S& fmt) {return (*this)(o, fmt);};
    template <class T1>
        Tprintf& operator()(std::ostream& o, const S& fmt, T1 a1) {return (*this)(o, fmt) % a1;};
    template <class T1, class T2>
        Tprintf& operator()(std::ostream& o, const S& fmt, T1 a1, T2 a2) {return (*this)(o, fmt) % a1, a2;};
    template <class T1, class T2, class T3>
        Tprintf& operator()(std::ostream& o, const S& fmt, T1 a1, T2 a2, T3 a3) {return (*this)(o, fmt) % a1, a2, a3;};
    template <class T1, class T2, class T3, class T4>
        Tprintf& operator()(std::ostream& o, const S& fmt, T1 a1, T2 a2, T3 a3, T4 a4) {return (*this)(o, fmt) % a1, a2, a3, a4;};

    template <class T1, class T2, class T3, class T4, class T5>
    Tprintf& operator()(std::ostream& o, const S& fmt, T1 a1, T2 a2, T3 a3, T4 a4, T5 a5)
    {return (*this)(o, fmt) % a1, a2, a3, a4, a5;};

    template <class T1, class T2, class T3, class T4, class T5, class T6>
    Tprintf& operator()(std::ostream& o, const S& fmt, T1 a1, T2 a2, T3 a3, T4 a4, T5 a5, T6 a6)
    {return (*this)(o, fmt) % a1, a2, a3, a4, a5, a6;};

    template <class T1, class T2, class T3, class T4, class T5, class T6, class T7>
    Tprintf& operator()(std::ostream& o, const S& fmt, T1 a1, T2 a2, T3 a3, T4 a4, T5 a5, T6 a6, T7 a7)
    {return (*this)(o, fmt) % a1, a2, a3, a4, a5, a6, a7;};

    template <class T1, class T2, class T3, class T4, class T5, class T6, class T7, class T8>
    Tprintf& operator()(std::ostream& o, const S& fmt, T1 a1, T2 a2, T3 a3, T4 a4, T5 a5, T6 a6, T7 a7, T8 a8)
    {return (*this)(o, fmt) % a1, a2, a3, a4, a5, a6, a7, a8;};

    template <class T1, class T2, class T3, class T4, class T5, class T6, class T7, class T8, class T9>
    Tprintf& operator()(std::ostream& o, const S& fmt, T1 a1, T2 a2, T3 a3, T4 a4, T5 a5, T6 a6, T7 a7, T8 a8, T9 a9)
    {return (*this)(o, fmt) % a1, a2, a3, a4, a5, a6, a7, a8, a9;};

    template <class T1, class T2, class T3, class T4, class T5, class T6, class T7, class T8, class T9, class T10>
    Tprintf& operator()(std::ostream& o, const S& fmt, T1 a1, T2 a2, T3 a3, T4 a4, T5 a5, T6 a6, T7 a7, T8 a8, T9 a9, T10 a10)
    {return (*this)(o, fmt) % a1, a2, a3, a4, a5, a6, a7, a8, a9, a10;};


    Tprintf& cerr(const S& fmt) {return (*this)(std::cerr, fmt);};
    template <class T1>
        Tprintf& cerr(const S& fmt, T1 a1) {return (*this)(std::cerr, fmt) % a1;};
    template <class T1, class T2>
        Tprintf& cerr(const S& fmt, T1 a1, T2 a2) {return (*this)(std::cerr, fmt) % a1, a2;};
    template <class T1, class T2, class T3>
        Tprintf& cerr(const S& fmt, T1 a1, T2 a2, T3 a3) {return (*this)(std::cerr, fmt) % a1, a2, a3;};
    template <class T1, class T2, class T3, class T4>
        Tprintf& cerr(const S& fmt, T1 a1, T2 a2, T3 a3, T4 a4) {return (*this)(std::cerr, fmt) % a1, a2, a3, a4;};

    template <class T1, class T2, class T3, class T4, class T5>
    Tprintf& cerr(const S& fmt, T1 a1, T2 a2, T3 a3, T4 a4, T5 a5)
    {return (*this)(std::cerr, fmt) % a1, a2, a3, a4, a5;};

    template <class T1, class T2, class T3, class T4, class T5, class T6>
    Tprintf& cerr(const S& fmt, T1 a1, T2 a2, T3 a3, T4 a4, T5 a5, T6 a6)
    {return (*this)(std::cerr, fmt) % a1, a2, a3, a4, a5, a6;};

    template <class T1, class T2, class T3, class T4, class T5, class T6, class T7>
    Tprintf& cerr(const S& fmt, T1 a1, T2 a2, T3 a3, T4 a4, T5 a5, T6 a6, T7 a7)
    {return (*this)(std::cerr, fmt) % a1, a2, a3, a4, a5, a6, a7;};

    template <class T1, class T2, class T3, class T4, class T5, class T6, class T7, class T8>
    Tprintf& cerr(const S& fmt, T1 a1, T2 a2, T3 a3, T4 a4, T5 a5, T6 a6, T7 a7, T8 a8)
    {return (*this)(std::cerr, fmt) % a1, a2, a3, a4, a5, a6, a7, a8;};

    template <class T1, class T2, class T3, class T4, class T5, class T6, class T7, class T8, class T9>
    Tprintf& cerr(const S& fmt, T1 a1, T2 a2, T3 a3, T4 a4, T5 a5, T6 a6, T7 a7, T8 a8, T9 a9)
    {return (*this)(std::cerr, fmt) % a1, a2, a3, a4, a5, a6, a7, a8, a9;};

    template <class T1, class T2, class T3, class T4, class T5, class T6, class T7, class T8, class T9, class T10>
    Tprintf& cerr(const S& fmt, T1 a1, T2 a2, T3 a3, T4 a4, T5 a5, T6 a6, T7 a7, T8 a8, T9 a9, T10 a10)
    {return (*this)(std::cerr, fmt) % a1, a2, a3, a4, a5, a6, a7, a8, a9, a10;};


    void lock();
    void unlock();
};

extern Tprintf tprintf;

template <class T>
Tprintf& Tprintf::process(T arg) {
    check();
    if (pos == format.end()) {
        dump(arg);
        return *this;
    }

    //fprintf(stderr, "pos = %c\n", *pos);
    //assert(*pos == '%');
    if (*pos == '%')
        ++pos;
    else
        fprintf(stderr, "EVIL: *pos != '%%' \n");

    fillnumfmt();

    dump(arg);

#ifdef PRINTF_COMPAT
    while (lastspec && *lastspec && pos != format.end() && *lastspec == *pos) {
        ++lastspec;
        ++pos;
    }
#endif

    if (pos != format.end()) {
        SIT start(pos);
        pos = find();
        for (; start != pos; ++start)
            *out << *start;
    }
    return *this;
}

/* for general case we can't use numfmt, so just output it */
template <class T>
void Tprintf::dump(T arg) {
    *out << arg;
    *out << numfmt;
}

/*
template <class T>
void dump(T *arg) {
    dump(static_cast<const void*>(arg));
}
template <class T>
void dump(const T *arg) {
    dump(static_cast<const void*>(arg));
}

template <>
void Tprintf::dump(const void* arg);
*/
template <>
void Tprintf::dump(signed char arg);
template <>
void Tprintf::dump(unsigned char arg);
template <>
void Tprintf::dump(short arg);
template <>
void Tprintf::dump(unsigned short arg);
template <>
void Tprintf::dump(int arg);
template <>
void Tprintf::dump(unsigned int arg);
template <>
void Tprintf::dump(long arg);
template <>
void Tprintf::dump(unsigned long arg);
#ifdef HAVE_LONG_LONG
template <>
void Tprintf::dump(long long arg);
template <>
void Tprintf::dump(unsigned long long arg);
#endif
template <>
void Tprintf::dump(float arg);
template <>
void Tprintf::dump(double arg);
#ifdef HAVE_LONG_DOUBLE
template <>
void Tprintf::dump(long double arg);
#endif

#endif
