/** $Id$ $URL$ */
#include "tprintf.h"
/**\file tprintf.cpp
 *
 * Templatised printf-like functions
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <sstream>
#include <stdarg.h>
#include <assert.h>
#include <SDL_thread.h>

Tprintf tprintf;

Tprintf::Tprintf(std::ostream& o, const S& fmt)
    : out(&o), format(fmt), prog(false), mut(0), lastspec(0)
{
}

Tprintf::Tprintf(const S& fmt)
: out(&std::cout), format(fmt), prog(false), mut(0), lastspec(0)
{
}

Tprintf& Tprintf::operator=(const Tprintf& rhs)
{
    out = (rhs.out);
    format = (rhs.format);
    prog = (rhs.prog);
    /* keep mut */ //mut = (0);
    lastspec = 0;
    return *this;
}

Tprintf::Tprintf(const Tprintf& rhs)
: out(rhs.out), format(rhs.format), prog(rhs.prog), mut(0), lastspec(0)
{
}

Tprintf::~Tprintf() {
    if (mut)
        SDL_DestroyMutex(mut);
}

void Tprintf::set(std::ostream& o, const S& fmt) {
    out = &o;
    format = fmt;
    prog = true;
    pos = format.begin();
    SIT start(pos);
    pos = find();
    for (; start != pos; ++start)
        *out << *start;
}
void Tprintf::check() {
    if (!prog) {
        pos = format.begin();
        prog = true;
    }
}
Tprintf& Tprintf::operator()(std::ostream& o, const S& fformat) {
    set(o, fformat);
    return *this;
}
Tprintf& Tprintf::operator()(const S& fformat) {
    /* each time we take a new format, reset to std::cout */
    set(std::cout, fformat);
    return *this;
}
bool Tprintf::next(S::value_type &c) {
    if (pos != format.end()) {
        c = *pos;
        return true;
    }
    return false;
}
bool Tprintf::after(S::value_type &c) {
    SIT p;
    if (pos != format.end() && (p = pos+1) != format.end()) {
        c = *p;
        return true;
    }
    return false;
}
Tprintf::SIT Tprintf::find() {
    S::value_type n = '?';
    SIT p(pos);
    for  (; (p != format.end() &&
             (*p != '%' || (after(n) && n == '%')));
          ++p)
        if (n == '%')
            ++p;
    return p;
}
Tprintf::SIT Tprintf::find(const S::value_type c) {
    SIT p(pos);
    for (; p != format.end() && *p != c; ++p)
        ;
    return p;
}

namespace {
#ifndef HAVE_VSNPRINTF
    int vsnprintf(char *str, size_t size, const char *format, va_list ap) {
        (void)size;
        return vsprintf(str, format, ap);
    }
#endif
#ifndef HAVE_ASPRINTF
    int vasprintf(char **strp, const char *fmt, va_list ap) {
        size_t amt = 8 /*padding*/ + 20 /*length of a long long*/ + strlen(fmt);
        const char* width = strstr(fmt, "0123456789");
        if (width)
            amt += atoi(width);
        *strp = static_cast<char*>(malloc(amt));
        if (*strp)
            return vsnprintf(*strp, amt, fmt, ap);
        return 0;
    }
#endif

    int my_asprintf(char **strp, const char *fmt, ...) {
        //fprintf(stderr, "asprintf(%s)\n", fmt);
        int r;
        va_list ap;
        va_start(ap, fmt);
        r = vasprintf(strp, fmt, ap);
        va_end(ap);
        return r;
    }
}

void Tprintf::lock() {
    if (!mut)
        mut = SDL_CreateMutex();
    SDL_mutexP(mut);
}

void Tprintf::unlock() {
    assert(mut && "unlock called before lock");
    SDL_mutexV(mut);
}

template <class T>
void Tprintf::dumpnum(T arg, const char* spec) {
    char *mstr;
    char *cfmt = static_cast<char*>(malloc(strlen(spec) + numfmt.size() + 2));
    cfmt[0] = '%';
    for (size_t i = 0; i < numfmt.size(); ++i)
        cfmt[i+1] = numfmt[i];
    cfmt[numfmt.size()+1] = '\0';
    strcat(cfmt, spec);

    if (my_asprintf(&mstr, cfmt, arg) && mstr) {
        /* "worked" */
        *out << mstr;
        lastspec = spec;
    } else {
        /* pretend numfmt is regular filler text */
        *out << arg;
        *out << numfmt;
        lastspec = 0;
    }
    if (mstr)
        free(mstr);
    free(cfmt);
}

template <>
void Tprintf::dump(const void* arg) {
    char voidstr[20];
    snprintf(voidstr, 20, "%p", arg);
    *out << voidstr;
    *out << numfmt;
}

template <>
void Tprintf::dump(signed char arg) {
    dumpnum(arg, "hhd");
}
template <>
void Tprintf::dump(unsigned char arg) {
    dumpnum(arg, "hhu");
}
template <>
void Tprintf::dump(short arg) {
    dumpnum(arg, "hd");
}
template <>
void Tprintf::dump(unsigned short arg) {
    dumpnum(arg, "hu");
}
template <>
void Tprintf::dump(int arg) {
    dumpnum(arg, "d");
}
template <>
void Tprintf::dump(unsigned int arg) {
    dumpnum(arg, "u");
}
template <>
void Tprintf::dump(long arg) {
    dumpnum(arg, "ld");
}
template <>
void Tprintf::dump(unsigned long arg) {
    dumpnum(arg, "lu");
}
#ifdef HAVE_LONG_LONG
template <>
void Tprintf::dump(long long arg) {
    dumpnum(arg, "lld");
}
template <>
void Tprintf::dump(unsigned long long arg) {
    dumpnum(arg, "llu");
}
#endif

template <>
void Tprintf::dump(float arg) {
    dumpnum(arg, "f");
}

template <>
void Tprintf::dump(double arg) {
    dumpnum(arg, "f");
}
#ifdef HAVE_LONG_DOUBLE
template <>
void Tprintf::dump(long double arg) {
    dumpnum(arg, "Lf");
}
#endif

